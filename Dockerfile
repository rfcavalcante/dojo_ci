FROM maven:3-jdk-8 
ADD target/dojoCI-0.0.1-SNAPSHOT.jar App.jar
ENTRYPOINT ["java", "-Djava,security,egd=file:/dev/./urandom","-jar","/app.jar"]
